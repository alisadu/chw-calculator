require('core-js/es6/map');
require('core-js');
//calc Controller

var calcController = (function(){

	var ConversionWeight = new Map([
		['kg', 1],
		['lb', 0.45359237],
	]);
	var ConversionDims = new Map([
		['cm', 1],
		['inch', 2.54],
	]);


	var transportModeMultiplier = new Map ([
		['AIR  (volume ratio 1:6) ', 6 ],
		['SEA  (volume ratio 1:1)', 1 ],
		['ROAD  (volume ratio 1:3)', 3],
	]);

	var Item = function(id, pcs, weight, length, width, height, weightUnit, dimsUnit){
		this.id = id;
		this.pcs = pcs;
		this.weight = weight * ConversionWeight.get(weightUnit);
		this.length = length * ConversionDims.get(dimsUnit);
		this.width = width * ConversionDims.get(dimsUnit);
		this.height = height * ConversionDims.get(dimsUnit);
		this.itemlVolume = this.pcs * this.length * this.width * this.height/1000000;
		this.itemlWeight = this.weight * this.pcs;
	};

	var data = {
		itemsArray:[],
		totals:{
			qty:0,
			grWeight:0,
			volume:0,              
		},
		transportMode:'',
		chWeight :0,
	}

	return {
		addItem: function(pcs, weight, l, w , h, wu, du){
			var newItem, id;

			//create new id, elem on last index +1
			if (data.itemsArray.length > 0){
				id = data.itemsArray[data.itemsArray.length - 1].id + 1;
			} else {
				id = 0;
			}
			//create new item
			newItem = new Item(id, pcs, weight, l, w, h, wu, du);

			//push into itemsArray
			data.itemsArray.push(newItem);

			return newItem;   
		},

		calculateTotals: function(){
			var sumQty, sumWeight, sumVolume;
			sumQty = 0;
			sumWeight = 0;
			sumVolume = 0;
			//here we calculate totals based on itemsArray values
			data.itemsArray.forEach(function(el){
				sumQty += el.pcs;
				sumWeight += el.itemlWeight;
				sumVolume += el.itemlVolume;
			});
			data.totals.qty = sumQty;
			data.totals.grWeight = sumWeight;
			data.totals.volume = sumVolume;

		},

		deleteItem: function(id){
			var ids, position;

			ids = data.itemsArray.map(function (cur){
				return cur.id;
			});
			position = ids.indexOf(id);

			if (position !== -1){
				data.itemsArray.splice(position, 1);
			}

		},

		deleteAll: function(){
			if (data.itemsArray.length >0){
				data.itemsArray = [];        
			}
		},

		calculateChWeight: function(mode){

			var volWeight = data.totals.volume * (1000/transportModeMultiplier.get(mode));

			if (volWeight >= data.totals.grWeight){
				data.chWeight = volWeight ;
			} else{
				data.chWeight = data.totals.grWeight;
			}

		},

		getTotals: function(){
			return {
				totalQty: data.totals.qty,
				totalWeight: data.totals.grWeight,
				totalVolume: data.totals.volume,
				transportMode: data.transportMode,
				chWeight: data.chWeight,
			}
		},

		getTransportModes: function(){
			var modeArr = [];
			transportModeMultiplier.forEach(function(value, key){
				modeArr.push(key);
			});
			return  modeArr;
		},

		getWeightUnits: function(){
			var modeArr = [];
			ConversionWeight.forEach(function(value, key){
				modeArr.push(key);
			});
			return  modeArr;
		},
		getDimsUnits: function(){
			var modeArr = [];
			ConversionDims.forEach(function(value, key){
				modeArr.push(key);
			});
			return  modeArr;
		},

		
		testing:function(){
			console.log(data);
		},
		
	};    

})();

//UI controller
var UIController =(function(){
	const DOMstrings ={

		inputQty: '.add__quantity',
		inputWeight: '.add__weight',
		inputLength: '.add__length', 
		inputWidth: '.add__width', 
		inputHeight: '.add__height', 
		inputBtn: '.add__btn',
		itemContainer: '.item__container',
		itemRow: '.item',
		labelTotalQty: '.total__qty__value',
		labelTotalWeight: '.total__weight__value',
		labelTotalVolume: '.total__volume__value',
		labelChWeight: '.total__chweight__value',
		inputTransportMode: '.transport__mode',
		clearAllBtn: '.clear__all__btn',
		selectDimsUnit: '#cm__inch',
		selectWeightUnit:'#kg__lb',
		errorContainer:'.error__container',
	};

	var nodeListForEach = function(list, callback){
		for (var i =0; i < list.length; i++) {
			callback(list[i], i);
		}
	};

	return {
		getInput: function(){
			return {
				qty:parseInt(document.querySelector(DOMstrings.inputQty).value),
				weight: parseFloat(document.querySelector(DOMstrings.inputWeight).value),
				length: parseFloat(document.querySelector(DOMstrings.inputLength).value),
				width:parseFloat(document.querySelector(DOMstrings.inputWidth).value),
				height:parseFloat(document.querySelector(DOMstrings.inputHeight).value),
				weightUnit:document.querySelector(DOMstrings.selectWeightUnit).value,
				dimsUnit:document.querySelector(DOMstrings.selectDimsUnit).value,
			};
		},

		getTransportMode: function(){           
			return document.querySelector(DOMstrings.inputTransportMode).value;
		},

		clearFields: function(){
			var fields, fieldsArray;

			// querySelectorAll returns list (not an array)
			document.querySelector (DOMstrings.inputQty).value = '1';
			fields = document.querySelectorAll(DOMstrings.inputWeight + ',' + DOMstrings.inputLength + ',' + DOMstrings.inputWidth + ',' + DOMstrings.inputHeight);

			//calling and array method on a list with 'call' 
			fieldsArray = Array.prototype.slice.call(fields);

			fieldsArray.forEach(function(current, index, array){
				current.value ='';
			});

			fieldsArray[0].focus();
		},

		addListItem: function(obj){
			var html, newHtml;

			html = '<div class="item" id="item-%id%"><div class="item__quantity"><span class="item__quantity__value">%qty%</span></div><div class="item__weight"><span class="item__weight__value">%weight%</span></div><div class="item__dims"><span class="item__dims__value">%dims%</span></div><div class="item__total__weight"><span class="total_weight__value">%itemWeight%</span></div><div class="item__total__volume"><span class="total_volume__value">%itemVolume%</span></div><div class="item__delete"><button class="item__delete__btn"><span><i class="ion-ios-close-outline"></span></i></button></div></div>';

			newHtml = html.replace('%id%', obj.id).replace('%qty%', obj.pcs).replace('%weight%', obj.weight.toFixed(1)).replace('%dims%', obj.length.toFixed(0) + 'x' + obj.width.toFixed(0) + 'x' + obj.height.toFixed(0)).replace('%itemWeight%', obj.itemlWeight.toFixed(1)).replace('%itemVolume%', obj.itemlVolume.toFixed(3));

			document.querySelector(DOMstrings.itemContainer).insertAdjacentHTML ('beforeend', newHtml);
		},

		deleteListItem : function(id){
			var element = document.getElementById(id);
			element.parentNode.removeChild(element);

		},
		deleteAllListItems: function(){
			var itemRows = document.querySelectorAll(DOMstrings.itemRow);
			nodeListForEach(itemRows, function(node){
				if (node.id !== 'item-header'){
					node.parentNode.removeChild(node);               
				}  
			}); 

		},

		updateTotals: function(obj){
			document.querySelector(DOMstrings.labelTotalQty).textContent = obj.totalQty.toFixed(0);
			document.querySelector(DOMstrings.labelTotalWeight).textContent = obj.totalWeight.toFixed(1);
			document.querySelector(DOMstrings.labelTotalVolume).textContent = obj.totalVolume.toFixed(3);
			document.querySelector(DOMstrings.labelChWeight).textContent = obj.chWeight.toFixed(1);;
		},
		
		getDOMstrings: function(){
			return DOMstrings;
		},
		
		showError:function(){
						var errorContainer = document.querySelector(DOMstrings.errorContainer);
			var errorMessage = document.createElement('span');
			errorMessage.textContent = 'Please check your input: one or multiple numbers are invalid or missing.'
			errorContainer.appendChild(errorMessage);
		},
		
		hideError:function(){
			var errorContainer = document.querySelector(DOMstrings.errorContainer);
			errorContainer.textContent = ''; 
		},
	};

})();

//global App Controller
var controller = (function(calcCtrl, UICtrl){

	const populateOptions = function(){

		const DOM = UICtrl.getDOMstrings();
		var select = document.querySelector(DOM.inputTransportMode);
		var unitDims = document.querySelector(DOM.selectDimsUnit);
		var unitWeight = document.querySelector(DOM.selectWeightUnit);


		calcCtrl.getTransportModes().forEach(function(element){
			var option = document.createElement('option');
			option.value = element;
			option.innerHTML = element;
			select.appendChild(option);
		});

		calcCtrl.getWeightUnits().forEach(function(element){
			var option = document.createElement('option');
			option.value = element;
			option.innerHTML = element;
			unitWeight.appendChild(option);
		});

		calcCtrl.getDimsUnits().forEach(function(element){
			var option = document.createElement('option');
			option.value = element;
			option.innerHTML = element;
			unitDims.appendChild(option);
		});
	};

	const setUpEventListeners = function(){
		const DOM = UICtrl.getDOMstrings();

		document.querySelector(DOM.inputBtn).addEventListener('click', ctrlAddItem);
		document.addEventListener('keypress', function(event){
			if ((event.keyCode === 13 || event.which === 13) && ((event.srcElement) ? event.srcElement.className !== "add__btn" : event.target.className !== "add__btn")) {
				ctrlAddItem();     
			}
		});
		document.querySelector(DOM.inputTransportMode).addEventListener('change', ctrlChangeTrMode);
		document.querySelector(DOM.itemContainer).addEventListener('click', ctrlDeleteItem);
		document.querySelector(DOM.clearAllBtn).addEventListener('click', ctrlInitialize);
	};

	var ctrlInitialize = function(){
		//clear all data from model
		//1. delete all items
		calcCtrl.deleteAll();

		//update totals, update chw
		calcCtrl.calculateTotals();
		calcCtrl.calculateChWeight(UICtrl.getTransportMode());
		var totals = calcCtrl.getTotals();
		UICtrl.updateTotals(totals);

		//update UI
		UICtrl.deleteAllListItems();

		//clear all unput fields
		UICtrl.clearFields();
		UICtrl.hideError();
	}

	var ctrlAddItem = function(){
		var input, newItem;

		//get user input data
		input = UICtrl.getInput();
		UICtrl.hideError();
		
		//validate input data
		if (isValidNumber(input.qty) && isValidNumber(input.weight) && isValidNumber(input.length) && isValidNumber(input.width) && isValidNumber(input.height)){
			
			//add item to calcController
			newItem = calcCtrl.addItem(input.qty, input.weight, input.length, input.width, input.height, input.weightUnit, input.dimsUnit);

			//add new Item to UI as well
			UICtrl.addListItem(newItem);


			//Clear input fields
			UICtrl.clearFields();

			//calculate and update totals
			calcCtrl.calculateTotals();
			calcCtrl.calculateChWeight(UICtrl.getTransportMode());
			var totals = calcCtrl.getTotals();
			UICtrl.updateTotals(totals);
		} else {
		
			UICtrl.showError();
		}

	};


	var ctrlDeleteItem = function(event){
		var itemId, id;
		itemId = event.target.parentNode.parentNode.id;
		if (itemId){
			id = parseFloat(itemId.split('-')[1]);

			//delete by Id from data structure
			calcCtrl.deleteItem(id);
			//delete from UI
			UICtrl.deleteListItem(itemId);

			//update  totals
			calcCtrl.calculateTotals();
			calcCtrl.calculateChWeight(UICtrl.getTransportMode());
			var totals = calcCtrl.getTotals();
			UICtrl.updateTotals(totals);
		}
	};

	var ctrlChangeTrMode  = function(){
		calcCtrl.calculateChWeight(UICtrl.getTransportMode());
		var totals = calcCtrl.getTotals();
		UICtrl.updateTotals(totals);
	};


	var isValidNumber = function(input){
		if(!isNaN(input) && input >=0){
			return true;
		} else {
			return false;
		}

	};

	return{
		init: function(){
			populateOptions();
			setUpEventListeners();
		}


	};

})(calcController, UIController);


controller.init();