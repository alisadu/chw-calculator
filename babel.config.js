const presets = [
  [
    "@babel/preset-env",
    {
	"useBuiltIns": "usage",
      targets: {
        edge: "17",
        firefox: "60",
        chrome: "67",
        safari: "11.1",
		ie: "9"
      },
    },
  ],
];
const plugins = [ "babel-plugin-transform-es3-member-expression-literals", "babel-plugin-transform-es3-property-literals"
	];

module.exports = { presets, plugins };