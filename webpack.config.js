var path = require('path');
var webpack = require('webpack');
module.exports = {
	entry:  './src/chwcalc.js',
	output: {
		path: path.resolve(__dirname, 'build'),
		filename: 'bundle.js'
	},
	module: {
		rules: [
			{
				test: /\.js$/,
				use: [{
					loader: 'es3ify-loader'
				}, {
					loader: 'babel-loader',
					query: {
						presets: ['@babel/preset-env']
					}

				}],

			}
		]
	},
	stats: {
		colors: true
	},
	devtool: 'source-map'
};